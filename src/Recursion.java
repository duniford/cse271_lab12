/**
 * Lab 13. Implementing recursion methods while using Gitlab.
 * 
 * @author Roric Dunifon
 *
 */

public class Recursion {
    
    /**
     * Recursively finds base to the n'th power when given two int values.
     * 
     * @param base first int value.
     * @param n second int value that is the power value.
     * @return a computed int value
     */
    public static int powerN(int base, int n) {
        // If 0 is entered it is always 1
        if (n == 0) {
            return 1;
        // Recursively finding base to the nth power
        } else {
            return base * powerN(base, n - 1);
        }
    }
    
    /**
     * Given that a triangle of blocks has a 1st row containing 1, 2nd row containing 2, 
     * 3rd row containing 3 and so on... this method finds the total number of blocks 
     * given an int row value.
     * 
     * @param row int value.
     * @return int value of total blocks.
     */
    public static int triangle(int row) {
        // If it is the first row it is only one
        if (row == 1) {
            return 1;
        }
        // Recursively count for rows
        return row + triangle(row - 1);
    }
}
